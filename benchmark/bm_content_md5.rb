# -*- encoding: binary -*-
require "./test/helper"
require "benchmark"

class TestBMContentMD5 < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/content-md5.ru", 1)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
    @bs = ENV['bs'] ? ENV['bs'].to_i : 1024 * 1024
    @count = ENV['count'] ? ENV['count'].to_i : 1000
    @cmd = %w(dd if=/dev/zero)
    @cmd << "bs=#@bs"
    @cmd << "count=#@count"
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_upload_with_md5
    rd, wr = IO.pipe
    pid = fork do
      $stdout.reopen(wr)
      rd.close
      wr.close
      exec(*@cmd)
    end
    wr.close
    @env["CONTENT_LENGTH"] = (@bs * @count).to_s
    @env["rack.input"] = rd
    input = HTTP_Spew::ContentMD5.new(@env)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = nil
    res = Benchmark.measure do
      rv = req.run(100000)
    end
    assert_equal 200, rv[0].to_i
    pid, status = Process.waitpid2(pid)
    assert status.success?
    p res
  end
end
