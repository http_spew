require "./test/helper"
require "benchmark"

class TestBMContentMD5InputSpray < Test::Unit::TestCase
  def setup
    @nr = 4
    @addr, @port, @srv = start_server("./test/content-md5.ru", @nr)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
    @bs = ENV['bs'] ? ENV['bs'].to_i : 1024 * 1024
    @count = ENV['count'] ? ENV['count'].to_i : 1000
    @cmd = %w(dd if=/dev/zero)
    @cmd << "bs=#@bs"
    @cmd << "count=#@count"
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_spray_with_md5
    rd, wr = IO.pipe
    pid = fork do
      $stdout.reopen(wr)
      rd.close
      wr.close
      exec(*@cmd)
    end
    wr.close
    @env["CONTENT_LENGTH"] = (@bs * @count).to_s
    @env["rack.input"] = rd
    input = HTTP_Spew::ContentMD5.new(@env)
    sprayer = HTTP_Spew::InputSpray.new(@env, @nr, input)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    reqs = sprayer.readers.map do |md5_input|
      HTTP_Spew::Request.new(@env, md5_input, @sockaddr)
    end
    assert_equal @nr, reqs.size
    rv = nil
    res = Benchmark.measure do
      rv = HTTP_Spew.wait_mt reqs.size, reqs, 3600
    end
    assert_equal @nr, rv.size
    rv.each do |resp|
      assert_equal 200, resp.response[0].to_i
    end
    pid, status = Process.waitpid2(pid)
    assert status.success?
    p res
  end
end
