# -*- encoding: binary -*-
require "./test/helper"

class TestContentMD5 < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/content-md5.ru", 1)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_upload_with_md5
    str = rand_data(123) * (8 * 1021 * 13)
    expect_md5 = [Digest::MD5.digest(str)].pack("m0")
    expect = "expect=#{expect_md5}\nreaded=#{expect_md5}\n"
    @env["CONTENT_LENGTH"] = str.size.to_s
    @env["rack.input"] = StringIO.new(str)
    input = HTTP_Spew::ContentMD5.new(@env)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = HTTP_Spew.wait 1, [req], 666
    assert_equal 200, rv[0].response[0].to_i
    body = ""
    req.each { |buf| body << buf }
    assert_equal body, expect
    assert_equal expect_md5, input.content_md5
    assert_equal 123 * 8 * 1021 * 13, input.bytes_digested
  end

  def test_upload_with_corrupt_md5
    str = rand_data(123) * (8 * 1021 * 13)
    expect = [Digest::MD5.digest(str)].pack("m").strip!
    @env["HTTP_CONTENT_MD5"] = expect
    str = rand_data(123) * (8 * 1021 * 13)
    @env["CONTENT_LENGTH"] = str.size.to_s
    @env["rack.input"] = StringIO.new(str)
    input = HTTP_Spew::ContentMD5.new(@env)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = HTTP_Spew.wait 1, [req], 3600
    assert_kind_of HTTP_Spew::ChecksumError, rv[0].error
  end

  def test_upload_with_corrupt_length
    str = rand_data(123) * (8 * 1021 * 13)
    expect = [Digest::MD5.digest(str)].pack("m").strip!
    @env["HTTP_CONTENT_MD5"] = expect
    str = rand_data(123) * (8 * 1021 * 13)
    @env["CONTENT_LENGTH"] = (str.size + 1).to_s
    @env["rack.input"] = StringIO.new(str)
    input = HTTP_Spew::ContentMD5.new(@env)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = HTTP_Spew.wait 1, [req], 3600
    assert_kind_of HTTP_Spew::LengthError, rv[0].error
  end

  def test_upload_with_valid_md5
    str = rand_data(123) * (8 * 1021 * 13)
    expect = [Digest::MD5.digest(str)].pack("m").strip!
    @env["HTTP_CONTENT_MD5"] = expect
    @env["CONTENT_LENGTH"] = str.size.to_s
    @env["rack.input"] = StringIO.new(str)
    input = HTTP_Spew::ContentMD5.new(@env)
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = HTTP_Spew.wait 1, [req], 3600
    assert_equal 200, rv[0].response[0].to_i
    body = ""
    req.each { |buf| body << buf }
  end

  def test_upload_with_md5_alt_input
    str = rand_data(123) * (8 * 1021 * 13)
    expect_md5 = [Digest::MD5.digest(str)].pack("m0")
    expect = "expect=#{expect_md5}\nreaded=#{expect_md5}\n"
    @env["CONTENT_LENGTH"] = str.size.to_s
    @env["rack.input"] = File.open("/dev/null", "rb")
    input = HTTP_Spew::ContentMD5.new(@env, StringIO.new(str))
    assert_nil @env["CONTENT_LENGTH"]
    assert_equal "chunked", @env["HTTP_TRANSFER_ENCODING"]
    req = HTTP_Spew::Request.new(@env, input, @sockaddr)
    rv = HTTP_Spew.wait 1, [req], 666
    assert_equal 200, rv[0].response[0].to_i
    body = ""
    req.each { |buf| body << buf }
    assert_equal body, expect
    assert_equal expect_md5, input.content_md5
    assert_equal 123 * 8 * 1021 * 13, input.bytes_digested
  end
end if HAVE_UNICORN
