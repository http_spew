# SHA1 checksum generator
bs = ENV['bs'] ? ENV['bs'].to_i : 16384
require 'digest/sha1'
require 'unicorn/preread_input'
use Unicorn::PrereadInput
class InputWrap < Struct.new(:input)
  def each
    buf = ""
    while buf = input.read(0x4000, buf)
      yield buf
    end
  end
end

app = lambda do |env|
  headers = {
    "Content-Type" => "application/octet-stream",
    "Content-Length" => env["CONTENT_LENGTH"].to_s,
  }
  [ 200, headers, InputWrap.new(env["rack.input"]) ]
end
run app
