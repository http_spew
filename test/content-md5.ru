# SHA1 checksum generator
bs = ENV['bs'] ? ENV['bs'].to_i : 4096
require 'digest/md5'
use Rack::ContentLength
use Rack::ContentType, "text/plain"
app = lambda do |env|
  return exit!(5) if env["HTTP_X_FAIL"] == "true"
  digest = Digest::MD5.new
  input = env['rack.input']
  if buf = input.read(bs)
    begin
      digest.update(buf)
    end while input.read(bs, buf)
  end

  expect = env['HTTP_CONTENT_MD5']
  readed = [ digest.digest ].pack('m').strip
  body = "expect=#{expect}\nreaded=#{readed}\n"
  status = expect == readed ? 200 : 500

  [ status, {}, [ body ] ]
end
run app
