# -*- encoding: binary -*-
require "./test/helper"

class TestMirror < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/mirror.ru", 4 , true)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
  end

  def test_mirror_small
    str = rand_data(128)
    expect = [ str ]
    req = []
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    rv = HTTP_Spew.wait(3, req, 666000)
    rv.each do |r|
      assert_nil r.error
      response = r.response
      headers = Rack::Utils::HeaderHash.new(response[1])
      assert_equal 128, headers["Content-Length"].to_i
      assert_equal 200, response[0].to_i
      tmp = []
      response[2].each { |x| tmp << x.dup }
      assert_equal expect, tmp
    end
  end

  # no bidirectional input support
  def test_mirror_big
    str = rand_data(128) * (8 * 1024 * 8)
    expect = str
    req = []
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    rv = HTTP_Spew.wait(3, req, 6000)
    rv.each do |r|
      assert_nil r.error
      response = r.response
      headers = Rack::Utils::HeaderHash.new(response[1])
      assert_equal str.size, headers["Content-Length"].to_i
      assert_equal 200, response[0].to_i
      tmp = ""
      response[2].each { |x| tmp << x }
      assert_equal expect, tmp
    end
  end
end if HAVE_UNICORN
