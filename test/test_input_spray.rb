# -*- encoding: binary -*-
require "./test/helper"

class TestInputSpray < Test::Unit::TestCase
  BUF = (rand_data(128) * 1024 * 8 * 4).freeze
  EXPECT = Digest::SHA1.hexdigest(BUF).freeze

  def setup
    io = StringIO.new(BUF)
    @env = {
      "rack.input" => io,
      "CONTENT_LENGTH" => io.size.to_s
    }
  end

  def test_spray_ok
    sprayer = HTTP_Spew::InputSpray.new(@env, 2)
    readers = sprayer.readers
    assert_equal 2, readers.size

    sha1 = Hash[readers.map { |rd| [ rd, Digest::SHA1.new ] } ]

    readers.delete_if do |rd|
      if buf = rd.read(0x10000)
        sha1[rd].update buf
        false
      else
        true
      end
    end until readers.empty?

    sha1.each_value { |dig| assert_equal EXPECT, dig.hexdigest }
    sprayer.instance_variable_get(:@pipes).each_value { |x| assert x.closed? }
  end

  def test_spray_one_reader_dies
    sprayer = HTTP_Spew::InputSpray.new(@env, 3)
    readers = sprayer.readers
    assert_equal 3, readers.size
    sha1 = Hash[readers.map { |rd| [ rd, Digest::SHA1.new ] } ]
    count = Hash[readers.map { |rd| [ rd, 0 ] } ]
    to_die = readers[0]
    readers.delete_if do |rd|
      if buf = rd.read(0x100)
        sha1[rd].update buf
        n = count[rd] += buf.size
        if rd == to_die && n >= 0x20000
          rd.close.nil?
        else
          false
        end
      else
        true
      end
    end until readers.empty?

    dead_sha1 = sha1.delete to_die
    assert EXPECT != dead_sha1.hexdigest
    sha1.each_value { |dig| assert_equal EXPECT, dig.hexdigest }
    sprayer.instance_variable_get(:@pipes).each_value { |x| assert x.closed? }
  end

  def test_spray_stream
    rd, wr = IO.pipe
    assert @env.delete("CONTENT_LENGTH")
    io = @env.delete("rack.input")
    thr = Thread.new("") do |buf|
      while buf = io.read(128, buf)
        wr.write buf
      end
      wr.close
      :ok
    end
    @env["rack.input"] = rd
    sprayer = HTTP_Spew::InputSpray.new(@env, 2)
    readers = sprayer.readers
    assert_equal 2, readers.size

    sha1 = Hash[readers.map { |x| [ x, Digest::SHA1.new ] } ]

    readers.delete_if do |x|
      assert ! x.respond_to?(:size)
      if buf = x.read(0x10000)
        sha1[x].update buf
        false
      else
        true
      end
    end until readers.empty?

    sha1.each_value { |dig| assert_equal EXPECT, dig.hexdigest }
    sprayer.instance_variable_get(:@pipes).each_value { |x| assert x.closed? }
    thr.join
    assert_equal :ok, thr.value
    assert ! rd.closed?
    assert wr.closed?
    ensure
      rd.close
  end
end
