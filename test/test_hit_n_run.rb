require "./test/helper"

class TestHitNRun < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/sha1.ru", 1)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_request_with_existing_socket
    sock = TCPSocket.new(@addr, @port)
    assert(BasicSocket === sock)
    req = HTTP_Spew::HitNRun.new(@env, nil, sock)
    assert_equal sock, req.to_io
    assert_nothing_raised { req.close }
    assert sock.closed?
  end

  def test_request_single
    req = HTTP_Spew::HitNRun.new(@env, nil, @sockaddr)
    sym = req.resume
    if sym == :wait_writable
      assert req.to_io.wait_writable(0.1)
      sym = req.resume
    end
    assert_equal HTTP_Spew::HitNRun::RESPONSE.object_id, sym.object_id
  end

  def test_request_loop
    req = HTTP_Spew::HitNRun.new(@env, nil, @sockaddr)
    until Array === (rv = req.resume)
      req.__send__(rv)
    end
    assert_equal HTTP_Spew::HitNRun::RESPONSE.object_id, rv.object_id
  end
end if HAVE_UNICORN
