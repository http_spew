require "./test/helper"

class TestRequest < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/sha1.ru", 1)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_request_with_existing_socket
    sock = TCPSocket.new(@addr, @port)
    req = HTTP_Spew::Request.new(@env, nil, sock)
    assert_equal sock, req.to_io
    assert_nothing_raised { req.close }
    assert sock.closed?
  end

  def test_request_single
    req = HTTP_Spew::Request.new(@env, nil, @sockaddr)
    sym = req.resume
    assert_kind_of(Symbol, sym)
    if sym == :wait_writable
      assert req.to_io.wait_writable(1)
      sym = req.resume
    end
    assert_equal :wait_readable, sym
    assert req.to_io.wait_readable(1)
    rv = req.resume
    assert_equal req, rv[2]
  end

  def test_request_loop
    req = HTTP_Spew::Request.new(@env, nil, @sockaddr)
    until Array === (rv = req.resume)
      req.to_io.__send__(rv) # wait_readable/wait_writable
    end
    assert_kind_of Array, rv
    assert_equal 3, rv.size
    assert_equal req, rv[2]
  end
end if HAVE_UNICORN
