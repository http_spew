# -*- encoding: binary -*-
$stderr.sync = $stdout.sync = true
Thread.abort_on_exception = true
require "test/unit"
require "digest/sha1"
require "stringio"
require "tmpdir"
require "socket"
require "rack"
require "tempfile"
$-w = true
require "http_spew"
HAVE_UNICORN = `which unicorn 2>/dev/null`.size > 0

def start_server(config, worker_processes = 4, rewindable_input = false)
  ENV["RACK_ENV"] = "deployment"
  addr = ENV["TEST_HOST"] || "127.0.0.1"
  sock = TCPServer.new(addr, 0)
  port = sock.addr[1]
  fifo = Tempfile.new("fifo")
  fifo_path = fifo.path
  fifo.close!
  system("mkfifo", fifo_path) or abort "mkfifo: #$?"
  cfg = Tempfile.new("unicorn.config")
  cfg.puts "worker_processes #{worker_processes}"
  cfg.puts "preload_app true"
  cfg.puts "rewindable_input #{rewindable_input}"
  cfg.puts <<EOF
after_fork do |s,w|
  w.nr == (s.worker_processes - 1) and File.open("#{fifo_path}", "w").close
end
EOF
  cfg.flush
  pid = fork do
    ENV["UNICORN_FD"] = sock.fileno.to_s
    redirect = { sock => sock } if RUBY_VERSION.to_f >= 2.0
    exec "unicorn", "-l#{addr}:#{port}", "-c#{cfg.path}", config, redirect
  end
  File.open(fifo_path).close
  File.unlink fifo_path
  [ addr, port, pid ]
end

def rand_data(nr)
  File.open("/dev/urandom", "rb") { |fp| fp.read(nr) }
end
