# -*- encoding: binary -*-
require "./test/helper"

class TestUpload < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/sha1.ru")
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_spew_upload_empty
    req = []
    req << HTTP_Spew::Request.new(@env, nil, @sockaddr)
    req << HTTP_Spew::Request.new(@env, nil, @sockaddr)
    req << HTTP_Spew::Request.new(@env, nil, @sockaddr)
    rv = HTTP_Spew.wait(3, req, 666)
    assert_equal 3, rv.size
    rv.each do |r|
      assert_nil r.error
      response = r.response
      assert_equal 200, response[0].to_i
      tmp = []
      response[2].each { |x| tmp << x.dup }
      assert_equal [ "da39a3ee5e6b4b0d3255bfef95601890afd80709\n" ], tmp
    end
  end

  def tmp_blob(str)
    tmp = Tempfile.new "blob"
    tmp.write str
    tmp.flush
    assert_equal str.size, tmp.size
    tmp.rewind
    @tmpfiles << tmp
    tmp
  end

  def test_spew_upload_big
    str = rand_data(128) * (8 * 1024 * 4)
    expect = [ Digest::SHA1.hexdigest(str) << "\n" ]
    req = []
    req << HTTP_Spew::Request.new(@env, tmp_blob(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, tmp_blob(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, tmp_blob(str), @sockaddr)
    rv = HTTP_Spew.wait(3, req, 666)
    assert_equal 3, rv.size
    rv.each do |r|
      assert_nil r.error
      response = r.response
      assert_equal 200, response[0].to_i
      tmp = []
      response[2].each { |x| tmp << x.dup }
      assert_equal expect, tmp
    end
  end

  def test_spew_upload_small
    str = rand_data(128)
    expect = [ Digest::SHA1.hexdigest(str) << "\n" ]
    req = []
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    rv = HTTP_Spew.wait(3, req, 666)
    assert_equal 3, rv.size
    rv.each do |r|
      assert_nil r.error
      response = r.response
      assert_equal 200, response[0].to_i
      tmp = []
      response[2].each { |x| tmp << x.dup }
      assert_equal expect, tmp
    end
  end

  def test_spew_upload_small_two_of_three
    str = rand_data(128)
    expect = [ Digest::SHA1.hexdigest(str) << "\n" ]
    req = []
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    rv = HTTP_Spew.wait(2, req, 666)
    assert_equal 3, rv.size
    rv[0, 2].each do |r|
      assert_nil r.error
      response = r.response
      assert_equal 200, response[0].to_i
      tmp = []
      response[2].each { |x| tmp << x.dup }
      assert_equal expect, tmp
      assert_nothing_raised { response[2].close }
      assert r.to_io.closed?
    end
    failed = rv[2]
    assert_kind_of(HTTP_Spew::ConnectionReset, failed.error)
    assert failed.to_io.closed?
  end

  def test_spew_upload_nonblock
    str = rand_data(128)
    req = []
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    req << HTTP_Spew::Request.new(@env, StringIO.new(str), @sockaddr)
    before = req.map(&:object_id)
    rv = HTTP_Spew.wait_nonblock!(3, req)
    while rv.nil? do
      rv = HTTP_Spew.wait_nonblock!(3, req)
    end
    assert_nil rv.uniq!
    assert rv.size > 0
    rv.map!(&:object_id)
    rv.each do |r|
      assert before.include?(r), "rv=#{rv.inspect} before=#{before.inspect}"
    end
  end
end if HAVE_UNICORN
