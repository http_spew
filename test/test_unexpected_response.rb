require "./test/helper"

class TestRequest < Test::Unit::TestCase
  def setup
    @addr, @port, @srv = start_server("./test/response_code.ru", 1)
    @sockaddr = Socket.pack_sockaddr_in(@port, @addr)
    @env = {
      "REQUEST_METHOD" => "PUT",
      "REQUEST_URI" => "/",
      "HTTP_HOST" => "example.com",
    }
    @tmpfiles = []
  end

  def teardown
    Process.kill(:QUIT, @srv)
    Process.waitpid2(@srv)
    @tmpfiles.each { |tmp| tmp.closed? or tmp.close! }
  end

  def test_request_with_existing_socket
    sock = TCPSocket.new(@addr, @port)
    req = HTTP_Spew::Request.new(@env, nil, sock)
    assert_equal sock, req.to_io
    assert_nothing_raised { req.close }
    assert sock.closed?
  end

  def test_request_single_unexpected
    io = StringIO.new("")
    req = HTTP_Spew::Request.new(@env, io, @sockaddr, { 201 => true })
    rv = req.run(4)
    assert_instance_of HTTP_Spew::UnexpectedResponse, rv
  end

  def test_request_single_expected
    io = StringIO.new("")
    @env["HTTP_X_RESPONSE_CODE"] = "201"
    req = HTTP_Spew::Request.new(@env, io, @sockaddr, { 201 => true })
    rv = req.run(4)
    assert_instance_of Array, rv
    assert_equal 201, rv[0].to_i
    buf = ""
    rv[2].each { |chunk| buf << chunk }
    assert_equal "da39a3ee5e6b4b0d3255bfef95601890afd80709\n", buf
  end
end if HAVE_UNICORN
