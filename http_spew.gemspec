manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{http_spew}
  s.version = (ENV['VERSION'] || '0.7.1').dup
  s.authors = ["http_spew hackers"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{http_spew-public@yhbt.net}
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://yhbt.net/http_spew/'
  s.summary = 'HTTP Spew - LAN-only HTTP spam^H^H^H^Hclient for Ruby'
  s.test_files = Dir["test/test_*.rb"]
  s.add_dependency(%q<kcar>, [ "~> 0.3", ">= 0.3.1"])
  s.required_ruby_version = '>= 2.3'
  s.licenses = %w(GPL-2.0+)
end
