# -*- encoding: binary -*-
#
# This is a OS-level pipe that overrides IO#read to provide
# IO#readpartial-like semantics while remaining Rack::Lint-compatible
# for EOF, meaning we return nil on EOF instead of raising EOFError.
class HTTP_Spew::ChunkyPipe < IO

  # other threads may force an error to be raised in the +read+
  # method
  attr_accessor :error

  class << self
    alias new pipe
  end

  # Override IO#read to behave like IO#readpartial, but still return +nil+
  # on EOF instead of raising EOFError.
  def read(len = 16384, buf = '')
    check_err!
    case read_nonblock(len, buf, exception: false)
    when nil
      return check_err! || close
    when :wait_readable
      wait_readable # retry
    else
      return buf
    end while true
  end

  def check_err!
    if defined?(@error)
      closed? or close
      raise @error
    end
    nil
  end
end
