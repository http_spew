# -*- encoding: binary -*-

# An even more horrible way to make HTTP requests, just make them without
# reading a response!
class HTTP_Spew::HitNRun < HTTP_Spew::Request

  # frozen response
  RESPONSE = [ 666,
    {
      "Content-Length" => "0".freeze,
      "Content-Type" => "hit/run".freeze
    }.freeze,
    [].freeze ].freeze

  # we close the connection once we're ready to read the response to
  # save bandwidth
  def read_response
    close
    RESPONSE
  end
end
