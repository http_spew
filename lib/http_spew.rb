# -*- encoding: binary -*-
require "io/wait"
require "kcar"

module HTTP_Spew
  autoload :ChunkyPipe, "http_spew/chunky_pipe"
  autoload :ContentMD5, "http_spew/content_md5"
  autoload :HitNRun, "http_spew/hit_n_run"
  autoload :InputSpray, "http_spew/input_spray"

  Error = Class.new(RuntimeError)
  TimeoutError = Class.new(Error)
  ConnectionReset = Class.new(Error)
  RequestError = Class.new(Error)
  UnexpectedResponse = Class.new(RequestError)
  ChecksumError = Class.new(Error)
  LengthError = Class.new(Error)
  NoWritersError = Class.new(Error)
  EOF = Class.new(EOFError)

  require_relative "http_spew/version"
  require_relative "http_spew/headers"
  require_relative "http_spew/request"
  require_relative "http_spew/class_methods"

  extend HTTP_Spew::ClassMethods
end
